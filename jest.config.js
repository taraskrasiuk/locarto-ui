module.exports = {
  collectCoverage: true,
  testPathIgnorePatterns: ['node_modules', 'dist', 'temp', 'setupTests.ts'],
  setupTestFrameworkScriptFile: "./src/setupTests.ts",
  collectCoverageFrom: [
    'src/**/*.{ts,tsx}',
    '!src/**/*.d.ts',
    '!src/**/*.styled.{ts,tsx}',
    '!src/**/mocks/**/*.ts',
    '!src/index.tsx',
    '!src/setupTests.ts',
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -130,
    },
  },
  clearMocks: true,
  roots: [
    "<rootDir>/src"
  ],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  transformIgnorePatterns: ['<rootDir>node_modules'],
  testRegex: '.*\\.(test|spec).(ts|tsx|js)$',
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  // Setup Enzyme
  snapshotSerializers: ["enzyme-to-json/serializer"],
};
