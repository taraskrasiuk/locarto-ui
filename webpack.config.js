const webpack = require('webpack');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const Visualizer = require('webpack-visualizer-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = (env) => ({
  entry: path.resolve(__dirname, 'temp/index.js'),

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]-bundle.js',
  },

  optimization: {
    minimize: true,
    mergeDuplicateChunks: true,
  },

  // module: {
  //   rules: [
  //     {
  //       test: /\.ts|.tsx/,
  //       loader: 'ts-loader',
  //       exclude: [/node_modules/],
  //     },
  //   ],
  // },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(env),
      },
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve(__dirname, 'src/index.html'),
    }),
    new CompressionPlugin({
      test: /\.js$|\.css$|\.html$/,
      minRatio: 0.8,
    }),
    new Visualizer(),
  ],
});
