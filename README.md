# LOCARTO

### Setup

Libs:

 save deps:
 
     - react
     - react-dom
     - react-router-dom
     - redux
     - redux-thunk
     - react-redux
     - react-test-renderer
     - lodash
     - axios
     - babel-polyfill
     
 dev deps
 
    - @babel/cli
    - @babel/core
    - @babel/plugin-proposal-class-properties
    - @babel/plugin-proposal-object-rest-spread
    - @babel/plugin-transform-regenerator
    - @babel/plugin-transform-runtime": "^7.4.3"
    - @babel/plugin-transform-typescript": "^7.2.0"
    - @babel/preset-env": "^7.2.0"
    - @babel/preset-react": "^7.0.0"
    - @babel/preset-typescript": "^7.1.0"
    - babel-core": "^7.0.0-bridge.0"

    - webpack
    - webpack-cli
    - webpack-dev-server
 
    - style-loader
    - css-loader
    - sass-loader
    - file-loader
    - babel-loader 
    - ts-loader
    - source-map-loader

    
    - webpack-dev-middleware
    - webpack-hot-middleware
    
    - html-webpack-plugin
    - compression-webpack-plugin
    - webpack-visualizer-plugin
    
    - typescript
    - prettier
    - husky
    - cross-env
    
    - jest
    - jest-enzyme
    - ts-jest
    - jest-canvas-mock
    - enzyme
    - enzyme-adapter-react-16
    - enzyme-to-json
 
    - @types/react
    - @types/react-dom
    - @types/enzyme
    - @types/enzyme-adapter-react-16
    - @types/jest
    - @types/react-redux
    
    - tslint
    - tslint-react
    - tslint-config-airbnb
    - tslint-config-prettier
    - tslint-eslint-rules
    - tslint-plugin-prettier


## Structure
--

| - components ( top shareable )
| - containers / views
| - store
| - api
