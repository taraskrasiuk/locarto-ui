import React, { ChangeEvent } from 'react';
import ReactDOM from 'react-dom';

// just for test
import axios from 'axios';

interface AppState {
  login: string;
  password: string;
  _id: string;
  profiles: any;
}
class App extends React.Component<{}, AppState> {
  static loginURL = 'https://api-test.locarto.uk/accounts/login';
  static profilesURL = 'https://api-test.locarto.uk/accounts/{id}/profiles';

  state = {
    login: '',
    password: '',
    _id: '',
    profiles: {},
  };

  login = ({ login: email, password }: { login: string; password: string }): Promise<any> => {
    return axios.post(App.loginURL, { email, password });
  };

  getProfiles = ({ id }: { id: string }): Promise<any> => {
    return axios.get(App.profilesURL.replace('{id}', id));
  };

  handleOnLoginChange = ({ target: { value: login } }: ChangeEvent<HTMLInputElement>) => {
    this.setState({ login });
  };

  handleOnPasswordChange = ({ target: { value: password } }: ChangeEvent<HTMLInputElement>) => {
    this.setState({ password });
  };

  handleOnLoginClick = () => {
    this.login(this.state).then(
      ({
        data: {
          account: { _id },
        },
      }) => {
        this.getProfiles({ id: _id }).then(profiles => {
          this.setState({ profiles, _id });
        });
      },
    );
  };

  render() {
    const { login, password, _id, profiles } = this.state;
    return (
      <div>
        <div>
          <p>
            <span>Email:</span>
            <input type="text" onChange={this.handleOnLoginChange} />
          </p>
        </div>
        <div>
          <p>
            <span>Password:</span>
            <input type="password" onChange={this.handleOnPasswordChange} />
          </p>
        </div>
        <div>
          <button disabled={!(login || password)} onClick={this.handleOnLoginClick}>
            login
          </button>
        </div>

        <div>
          {_id ? <span>SUCCESS</span> : ''}
          {_id && profiles ? (
            <div>
              <h4>Profiles</h4>
              <p>{JSON.stringify(profiles)}</p>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
