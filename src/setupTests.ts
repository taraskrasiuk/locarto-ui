/**
 * setupTests module is loaded by Create-React-App before Jest testing,
 * the file name and location is conventional.
 */
import 'jest-enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-canvas-mock';

configure({ adapter: new Adapter() });
